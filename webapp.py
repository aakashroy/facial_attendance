import os
import re

from flask import Flask, render_template, request, send_from_directory
from ipcqueue import posixmq
from werkzeug.utils import secure_filename

import settings
from helpers.helper_functions import mkdir_p
from helpers.logging import SEVERITY_DEBUG, log
from prediction import prediction

app = Flask(__name__, template_folder=os.path.abspath(settings.WEBAPP_TEMPLATE_LOCATION))
app.config["UPLOAD_FOLDER"] = os.path.abspath(settings.WEBAPP_TMP_PHOTO_UPLOAD_LOCATION)


@app.route("/")
@app.route("/index")
def index():
    return render_template("index.html")


@app.route('/css/<path:path>')
def send_css(path):
    return send_from_directory('templates/css', path)

@app.route("/register", methods=["GET", "POST"])
def register():
    log(SEVERITY_DEBUG, "REGISTER", "Entered Function")
    if request.method == 'POST':
        log(SEVERITY_DEBUG, "REGISTER", "METHOD IS POST")
        employee_id = request.form["emp_id"]
        employee_id = re.sub(r"\s+", "", employee_id)
        data_valid = len(employee_id) > 0
        directory_path = ""

        if data_valid:

            directory_path = os.path.abspath(
                settings.WEBAPP_PERSISTANT_PHOTO_UPLOAD_LOCATION) + "/" + employee_id

            if (mkdir_p(directory_path)):
                log(SEVERITY_DEBUG, "REGISTER", "CREATE DIRECTORY", {"path": directory_path})

                for i in range(0, settings.MAX_PHOTOS_FOR_TRAINING):
                    data_valid = True
                    image_i = request.files["image_" + str(i + 1)]
                    if image_i:
                        image_i_secure = secure_filename(image_i.filename)
                        image_i_ext = image_i_secure.split(".")[-1]
                        final_img_path = os.path.abspath(
                            directory_path + "/" + employee_id + "_" + str(i + 1) + "." + image_i_ext)
                        log(SEVERITY_DEBUG, "REGISTER", "SAVING IMAGE", {"path": final_img_path})
                        image_i.save(final_img_path)
                        log(SEVERITY_DEBUG, "REGISTER", "IMAGE SAVED", {"path": final_img_path})
                    else:
                        log(SEVERITY_DEBUG, "REGISTER", "IMAGE ERROR")
                        data_valid = False
                        break
            else:
                return "You are already a registered user. <a href='/'>Click Here</a> to go back."

        if data_valid:
            q = posixmq.Queue(settings.POSIX_FACE_RECOGNITION_REGISTRATION_QUEUE)
            q.put((employee_id, directory_path))

            log(SEVERITY_DEBUG, "REGISTER", "REQUEST QUEUED")
            return "Registration process initiated. <a href='/'>Click Here</a> to go back."
        else:
            return "Registration Error. <a href='register'>Click Here</a> to start again."
    else:
        log(SEVERITY_DEBUG, "REGISTER", "METHOD IS GET")
        return render_template("register.html")


@app.route("/punch", methods=["GET", "POST"])
def punch():
    if request.method == 'POST':
        image_1 = request.files["image_1"]
        if image_1:
            image_1_secure = secure_filename(image_1.filename)
            final_img_path = os.path.abspath(settings.PUNCH_IMAGES_PATH + "/" + image_1_secure)
            image_1.save(final_img_path)
            nasc = prediction(final_img_path)
            name = nasc[0]
            score = nasc[1]
            if score > 0.50:
                return "Your attendance has been marked, " + name + " | CS: " + str(score)
            else:
                return "You are not a registered candidate. Please register <a href='register'>here</a>." + " | CS: " + str(
                                score)
        else:
            return "Punch Error. <a href='register'>Click Here</a> to start again."

    # else:
    #     return "No Candidates Registered. <a href='register'>Click Here</a> to register first."
    else:
       return render_template("punch.html")

if __name__ == "__main__":
    app.run()
