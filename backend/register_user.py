import glob
import os
import time

import cv2
import numpy as np
import pandas as pd
from google.cloud import automl_v1beta1 as automl
from ipcqueue import posixmq
from keras.preprocessing.image import ImageDataGenerator

import settings
from helpers.get_training_status import get_training_status
from helpers.logging import log, SEVERITY_DEBUG


def registration_worker():
    log(SEVERITY_DEBUG, "REGISTRATION WORKER", "Establish Q Connection")
    q = posixmq.Queue(settings.POSIX_FACE_RECOGNITION_REGISTRATION_QUEUE)
    log(SEVERITY_DEBUG, "REGISTRATION WORKER", "Q Connection Established")
    while True:
        items = []
        if q.qsize():
            while q.qsize() > 0:
                log(SEVERITY_DEBUG, "REGISTRATION WORKER", "Q Elements: {0}".format(q.qsize()))
                items.append(q.get())
            process_items(items)
        else:
            log(SEVERITY_DEBUG, "REGISTRATION WORKER", "Q Empty")
            time.sleep(1)


def process_items(items):
    log(SEVERITY_DEBUG, "PROCESS ITEMS", "Items to process: {0}".format(len(items)))
    for (employee_id, directory_path) in items:
        # Image Augmentation
        filenames = glob.glob(directory_path + '/*.jpg')
        datagen = ImageDataGenerator(rotation_range=35,
                                     width_shift_range=0.1,
                                     height_shift_range=0.1,
                                     shear_range=0.01,
                                     zoom_range=[0.5, 1.5],
                                     horizontal_flip=True,
                                     vertical_flip=False)

        imagenames_list = []
        read_images = []
        for f in filenames:
            imagenames_list.append(f)

        log(SEVERITY_DEBUG, "PROCESS ITEMS", "Images Found: {0}".format(len(imagenames_list)))

        for image in imagenames_list:
            read_images.append(cv2.imread(image, cv2.IMREAD_UNCHANGED)[:, :, ::-1])

        x_train = np.asarray(read_images)

        # for i in range(0, settings.NUM_IMAGES_TO_AUGMENT):
        #     datagen.flow(x_train, batch_size=1, save_to_dir=directory_path,
        #                  save_prefix='aug',
        #                  save_format='jpg')
        i = 0
        for batch in datagen.flow(x_train, batch_size=1, save_to_dir=directory_path,
                                  save_prefix='aug',
                                  save_format='jpg'):
            i += 1
            if i > settings.NUM_IMAGES_TO_AUGMENT:
                break

        # ################################################################################################################################
        # # uploading data to cloud
        oscmd = settings.GSUTIL_CLOUD_UPLOAD_COMMAND.format(directory_path, settings.BASE_GCS_PATH)
        os.system(oscmd)
        ##########################################################################################################################
        model_id = None
        # Preparing the CSV File
        list_fn = []
        list_fn.append(employee_id)
        filenames = [os.listdir(directory_path)]
        log(SEVERITY_DEBUG, "PROCESS ITEMS", "Total Images (Including Augmented): {0}".format(len(filenames[0])))
        files_dict = dict(zip(list_fn, filenames))
        data_array = []

        for (dict_key, files_list) in files_dict.items():
            for filename in files_list:
                # ToDo: Improve this condition
                if '.jpg' not in filename and '.jpeg' not in filename and '.png' not in filename:
                    continue  # don't include non-photos
                label = dict_key
                data_array.append((settings.BASE_GCS_PATH + dict_key + '/' + filename, label))

        dataframe = pd.DataFrame(data_array)
        csv_upload_path = directory_path + "/" + employee_id + ".csv"
        csv_cloud_path = settings.BASE_GCS_PATH + employee_id + "/" + employee_id + ".csv"
        dataframe.to_csv(csv_upload_path, index=False, header=False)

        # uploading csv to cloud

        oscmd = settings.GSUTIL_CLOUD_UPLOAD_COMMAND.format(csv_upload_path, csv_cloud_path)
        os.system(oscmd)

        client = automl.AutoMlClient()

        # Get the full path of the dataset.
        dataset_full_id = client.dataset_path(
            settings.PROJECT_ID, settings.COMPUTE_REGION, settings.DATASET_ID
        )

        # Get the multiple Google Cloud Storage URIs.
        input_uris = csv_cloud_path.split(",")
        input_config = {"gcs_source": {"input_uris": input_uris}}

        # Import data from the input URI.
        response = client.import_data(dataset_full_id, input_config)

        log(SEVERITY_DEBUG, "PROCESS ITEMS", "Processing Import")
        # synchronous check of operation status.
        log(SEVERITY_DEBUG, "PROCESS ITEMS", "Data imported. {}".format(response.result()))
        ###########################################################################################################################

        # creating and training the model
        try:
            csvfile = pd.read_csv("famoit.csv", sep=',', index_col=None)
            model_version = csvfile['Iterator'][0]
            old_model_id = csvfile['ModelID'][0]
        except:
            model_version = 0
            old_model_id = None
        model_name = 'facial_attendance' + str(model_version + 1)
        model_version = model_version + 1

        # A resource that represents Google Cloud Platform location.
        project_location = client.location_path(settings.PROJECT_ID, settings.COMPUTE_REGION)

        # Set model name and model metadata for the image dataset.
        my_model = {
            "display_name": model_name,
            "dataset_id": settings.DATASET_ID,
            "image_classification_model_metadata": {"train_budget": settings.TRAIN_BUDGET}
        }

        log(SEVERITY_DEBUG, "PROCESS ITEMS", "Creating Model", {'model_meta': my_model, 'proj_loc': project_location})

        response = client.create_model(project_location, my_model)

        start = time.time()
        while True:
            log(SEVERITY_DEBUG, "PROCESS ITEMS", "Checking training status . . .")
            training_status = get_training_status(response.operation.name)
            if "done" in training_status:
                if training_status["done"] and training_status.get("name"):
                    end = time.time()
                    model_id = training_status['response']['name'].split("/")[-1]
                    csvfile = pd.DataFrame({'ModelID': [model_id], 'Iterator': [model_version]})
                    csvfile.to_csv("famoit.csv", index=False, header=True)
                    log(SEVERITY_DEBUG, "PROCESS ITEMS",
                        "Training complete. Model Created. Took: {0} seconds".format(end - start))
                else:
                    raise IOError("Model training failed")
                break
            time.sleep(1)

        #######################################################################################################################

        if old_model_id:
            # Get the full path of the model.
            model_full_id = client.model_path(settings.PROJECT_ID, settings.COMPUTE_REGION, old_model_id)

            # Delete a model.
            response = client.delete_model(model_full_id)

            # synchronous check of operation status.
            log(SEVERITY_DEBUG, "PROCESS ITEMS", "Model deleted. {}".format(response.result()))
        else:
            log(SEVERITY_DEBUG, "PROCESS ITEMS", "Old Model does not exist")


if __name__ == "__main__":
    os.environ["GOOGLE_APPLICATION_CREDENTIALS"] = os.path.abspath(settings.GCS_JSON_SETTINGS)
    os.environ["PROJECT_ID"] = settings.PROJECT_ID
    os.environ["REGION_NAME"] = settings.COMPUTE_REGION
    registration_worker()
    # process_items([('24103', '/home/aakash/Desktop/facial_attendance/training_images/24103')])
