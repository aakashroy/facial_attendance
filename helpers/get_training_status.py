import json
import os

import requests
import subprocess as sp


# os.environ["GOOGLE_APPLICATION_CREDENTIALS"] = "/home/kuldeep/Downloads/Test-Project-8d74a26c4665.json"

def get_training_status(operation_name):
    output = sp.getoutput('gcloud auth application-default print-access-token')
    headers = {
        "Authorization": "Bearer " + output,
        "Content-Type": "application/json"
    }
    r = requests.get("https://automl.googleapis.com/v1beta1/{0}".format(operation_name), headers=headers)
    return r.json()
